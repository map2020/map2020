<?php

// Autoload model classes
spl_autoload_register(function($className) {
	require_once "{$_SERVER["DOCUMENT_ROOT"]}/../include/models/$className.php";
});

require_once "{$_SERVER['DOCUMENT_ROOT']}/../include/util.php";
require_once "{$_SERVER['DOCUMENT_ROOT']}/../include/database.php";

// Initialize session

session_set_cookie_params(["samesite" => "Lax"]);
session_start();

if (!isset($_SESSION["own_pins"])) {
	$_SESSION["own_pins"] = [];
}

// Allow API calls from anywhere
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: OPTIONS, GET, POST, PATCH, DELETE");
header("Access-Control-Allow-Headers: Accept, Accept-Language, Content-Language, Content-Type");

// Route

$path = explode("/", parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH));
$endpoint = $path[2];

if (strlen($endpoint) === 0) {
	respondOK(["ok" => true]);
} else if (file_exists($route = "{$_SERVER['DOCUMENT_ROOT']}/../include/routes/$endpoint.php")) {
	require_once $route;
} else {
	respondError([
		"error" => "route not found",
		"route" => $endpoint,
	], 404);
}
