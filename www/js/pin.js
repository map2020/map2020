"use strict";

/**
 * @typedef {{
 *     id: number,
 *     lat: number,
 *     lng: number,
 *     dt: int,
 *     title: string,
 *     body: string,
 *     type: string,
 *     own: string,
 *     marker: Object,
 * }} Pin
 */

let pinEditDialog;

$(() => {
    pinEditDialog = {
        dialog: $("#pinEditor"),
        type: $("#pinEditType"),
        title: $("#pinEditTitle"),
        body: $("#pinEditBody"),
        lat: undefined,
        lng: undefined,
        dt: undefined,
    };

    pinEditDialog.dialog.find("form").on("submit", event => {
        event.preventDefault();
        savePin();
    });
});

/**
 * Get a list of pins from the server and
 * add new ones to the map.
 */
function getPins() {
    const bounds = map.getBounds();
    const query = new URLSearchParams();
    query.append("minLat", bounds._northEast.lat);
    query.append("minLng", bounds._northEast.lng);
    query.append("maxLat", bounds._southWest.lat);
    query.append("maxLng", bounds._southWest.lng);

    fetch("/api/pins?" + query.toString()).then(r => r.json()).then(data => {
        if (data.hasOwnProperty("error")) {
            alert(data["error"]);
            return;
        }

        for (let pin of data) {
            if (!pins.hasOwnProperty(pin.id)) {
                pinToMap(pin);
                pins[pin.id] = pin;
            }
        }
    });
}

/**
 * Show the pin editor.
 * @param {Pin | number} pin - pre-filled data or the ID of an existing pin
 */
function editPin(pin) {
    if (!isNaN(pin)) {
        // Look up by ID
        pin = pins[pin];
        pinEditDialog.id = pin.id;
    } else {
        pinEditDialog.id = null;
    }

    pinEditDialog.type.val(pin.type ? pin.type : "generic");
    pinEditDialog.title.val(pin.title ? pin.title : "");
    pinEditDialog.body.val(pin.body ? pin.body : "");
    pinEditDialog.lat = pin.lat ? pin.lat : 0;
    pinEditDialog.lng = pin.lng ? pin.lng : 0;
    pinEditDialog.dt = pin.dt ? pin.dt : Math.floor(Date.now() / 1000); // JS uses milliseconds instead of seconds
    pinEditDialog.dialog.show();
}

/**
 * Clear and close the pin editor.
 */
function cancelEdit() {
    if (
        (pinEditDialog.title.val() || pinEditDialog.body.val()) &&
        !window.confirm("Are you sure you want to discard your edits?")
    ) {
        return;
    }

    pinEditDialog.dialog.hide();
}

/**
 * Upload pin data from the editor to the server.
 */
function savePin() {
    pinEditDialog.dialog.hide();

    let pin = {
        type: pinEditDialog.type.val(),
        title: pinEditDialog.title.val(),
        body: pinEditDialog.body.val(),
        lat: pinEditDialog.lat,
        lng: pinEditDialog.lng,
        own: true,
        dt: pinEditDialog.dt,
    };

    if (pinEditDialog.id) {
        pin.id = pinEditDialog.id;
    }

    fetch("/api/pins", {
        method: pin.id ? "PATCH" : "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(pin),
    }).then(r => r.json()).then(data => {
        if (data.hasOwnProperty("error")) {
            alert(data["error"]);
            return;
        }

        // Remove if existing pin
        if (pin.id) {
            map.removeLayer(pins[pin.id].marker);
        }

        // Get ID assigned if new pin
        if (data.hasOwnProperty("new_pin_id")) {
            pin.id = data["new_pin_id"];
        }

        // Save new pin data and add to map
        pins[pin.id] = pin;
        pinToMap(pin);
    });
}

/**
 * Delete a pin from the server.
 * @param {int} id - The ID of the pin to delete
 */
function deletePin(id) {
    if (!window.confirm("Are you sure you want to delete this pin? There is no undo!")) {
        return;
    }

    fetch("/api/pins?id=" + id, {
        method: "DELETE",
    }).then(r => r.json()).then(data => {
        if (data.hasOwnProperty("error")) {
            alert(data["error"]);
            return;
        }

        map.removeLayer(pins[id].marker);
        delete pins[id];
    });
}
