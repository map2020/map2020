"use strict";

let map;
let pins = {};
let mapClick = undefined;

function icon(file) {
    return L.Icon.extend({
        options: {
            iconUrl: file,
            iconSize: [32, 32],
            iconAnchor: [16, 16],
        }
    });
}

const icons = {
    "generic": L.Icon.Default,
    "protest": icon("img/police.png"),
    "violence": icon("img/fire.png"),
    "covid-19": icon("img/health.png"),
};

$(() => {
    map = L.map("map")
        .setView([35, -95], 5);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    getPins();
    map.on("zoomend, moveend", () => getPins());
    map.on("click", (event) => {
        if (!mapClick) {
            mapClick = setTimeout(() => {
                mapClick = undefined;
                editPin({
                    lat: event.latlng.lat,
                    lng: event.latlng.lng,
                });
            }, 200);
        } else {
            clearTimeout(mapClick);
            mapClick = undefined;
        }
    });

    $("#welcome").show();
});

function start() {
    $("#welcome").hide();

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((position) => {
            map.setView([position.coords.latitude, position.coords.longitude], 10);
        });
    }
}

function pinToMap(pin) {
    let tmp = document.createElement("div");
    tmp.innerHTML = pin.title;
    let title = tmp.textContent;

    let body = DOMPurify.sanitize(marked(pin.body));
    let time = $.timeago(new Date(Math.floor(pin.dt * 1000))); // JS uses milliseconds instead of seconds

    let content =
        '<div class="pin-header">' +
        '  <div class="pin-title">' + title + '</div>' +
        '  <time class="pinTime" datetime="' + pin.dt + '">' + time + '</time>' +
        '  - <a href="mailto:abuse@map2020.live?subject=Report%20pin%20#' + pin.id + '">Report</a>' +
        '</div>' +
        '<div class="pinBody">' + body + '</div>';

    if (pin.own) {
        content +=
            '<div class="col-2 text-center">' +
            '  <div class="img-link">' +
            '    <img src="img/pencil.png" width="32" height="32" alt="">' +
            '    <a href="javascript:editPin(' + pin.id + ')">Edit</a>' +
            '  </div>' +
            '  <div class="img-link">' +
            '    <img src="img/delete.png" width="32" height="32" alt="">' +
            '    <a href="javascript:deletePin(' + pin.id + ')">Delete</a>' +
            '  </div>' +
            '</div>';
    }

    pin.marker = L.marker([pin.lat, pin.lng], {
        title: pin.title,
        icon: new icons[pin.type](),
    })
        .addTo(map)
        .bindPopup(content);
}
