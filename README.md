# Map2020

Have an idea? Found a bug? Issues are welcome!

## API

If you would like to write an alternative frontend or extract data, you may use the same
REST API as the default client. Note that this API is meant for internal use and may
change at any time.

API endpoints return:
- 200 when successful
- 400 when parameters are invalid or missing
- 500 when there is a server/database error

### `GET /api/pins` - Get a list of pins in a region of the world map.

#### Query parameters

  - `minLat` - lower latitude bound
  - `maxLat` - upper latitude bound
  - `minLng` - lower longitude bound
  - `maxLng` - upper longitude bound
  
#### Response JSON list item structure

  - `id` - unique numeric ID of the pin
  - `dt` - the creation date and time of the pin as seconds since UNIX epoch
  - `type` - an item from `"generic", "protest", "violence", "covid-19"`
  - `title` - the short title of the pin
  - `body` - full Markdown content of the pin
  - `lat` - latitude of the pin
  - `lng` - longitude of the pin
  - `own` - boolean indicating whether the current user can edit this pin
  
#### Error response JSON

  - `error` - error text for the user
  
### `POST /api/pins` - Create a new pin

#### Request JSON

  - `type` - an item from `"generic", "protest", "violence", "covid-19"`
  - `title` - the short (100 characters or less) title for the pin
  - `body` - the Markdown content for the pin popup
  - `lat` - the latitude at which to place the pin
  - `lng` - the longitude at which to place the pin
  
#### Response JSON

  - `new_pin_id` - the ID assigned to the newly created pin
  
#### Error response JSON

  - `error` - error text for the user
  
### `PATCH /api/pins` - Edit an existing pin

#### Request JSON
  
  _Note that with the current API implementation, all of the request JSON field must be present._
  
  - `id` - the ID of the existing pin to edit
  - `type` - a new type for the pin
  - `title` - a new title for the pin
  - `body` - a new Markdown body for the pin
  
#### Error response JSON

  - `error` - error text for the user

## Project structure

### Database schema

```mysql
create table Pin
(
    id    int auto_increment,
    lat   double         not null,
    lng   double         not null,
    dt    datetime       null,
    title varchar(100)   not null,
    body  varchar(10000) not null,
    type  varchar(10)    not null,
    constraint Pin_id_uindex
        unique (id)
);

alter table Pin
    add primary key (id);
```

### Running

To run the application, create a file named `db.php` in the project
root (just outside `www`) that looks like this:

```php
<?php

define("DB_DSN", 'mysql:host=localhost;dbname=Map2020');
define("DB_USERNAME", '');
define("DB_PASSWORD", '');
```

Sessions are used to keep a list of pins that a given user has created
so that they can edit their own pins as long as the session lasts.

### Files

- `www` contains the document root (served at `/`)
  - `api` contains the code for the api (served at `/api/`)
    - `index.php` is a router script
    - `routes` contains one file for each route (`/api/<route>`)

# Attributions

- Map data: [OpenStreetMap](https://www.openstreetmap.org/copyright)
- Map renderer: [Leaflet](https://leafletjs.com/)
- Icons: [FatCow "Farm-Fresh" Web Icons](https://www.fatcow.com/free-icons)
