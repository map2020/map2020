<?php

switch ($_SERVER["REQUEST_METHOD"]) {
	case "OPTIONS":
		header("Allow: OPTIONS, GET, POST, PATCH, DELETE");
		respondOK([], 204);
		break;
	case "GET":
		listPins();
		break;
	case "POST":
		newPin();
		break;
	case "PATCH":
		editPin();
		break;
	case "DELETE":
		deletePin();
		break;
	default:
		respondInvalidMethod(["GET", "POST", "PATCH", "DELETE"]);
		break;
}

function listPins()
{
	global $pdo;

	$own_pin = count($_SESSION["own_pins"]) === 0 ? "FALSE" :
		"id IN (" . implode(",", $_SESSION["own_pins"]) . ")";
	$stmt = $pdo->prepare(<<<SQL
SELECT id as arrkey, id, dt, type, title, body, lat, lng, $own_pin as own
FROM Pin
WHERE lat BETWEEN :maxLat AND :minLat
  AND lng BETWEEN :maxLng AND :minLng
ORDER BY dt DESC
LIMIT 100
SQL
	);

	$stmt->bindValue(":minLat", getRequiredFloat("minLat"));
	$stmt->bindValue(":minLng", getRequiredFloat("minLng"));
	$stmt->bindValue(":maxLat", getRequiredFloat("maxLat"));
	$stmt->bindValue(":maxLng", getRequiredFloat("maxLng"));

	if (!$stmt->execute()) {
		error_log("Error getting pins: " . $stmt->errorCode());
		respondError([
			"error" => "failed to get pins",
		]);
	}

	$pins = $stmt->fetchAll(PDO::FETCH_CLASS, "Pin");
	respondOK($pins);
}

function newPin()
{
	global $pdo;

	$data = json_decode(file_get_contents("php://input"), true);
	checkPinType($type = getRequiredArg("type", $data));

	$stmt = $pdo->prepare(<<<SQL
INSERT INTO Pin (dt, lat, lng, type, title, body)
VALUES (UTC_TIMESTAMP(), :lat, :lng, :type, :title, :body)
SQL
	);

	$stmt->bindValue(":type", $type);
	$stmt->bindValue(":title", htmlspecialchars(getRequiredArg("title", $data)));
	$stmt->bindValue(":lat", getRequiredFloat("lat", $data));
	$stmt->bindValue(":lng", getRequiredFloat("lng", $data));
	$stmt->bindValue(":body", getRequiredArg("body", $data));

	if (!$stmt->execute()) {
		error_log("Error adding pin: " . trim($stmt->errorCode()));
		respondError([
			"error" => "failed to add pin",
		]);
	}

	$newPinID = $pdo->lastInsertId("Pin_id_uindex");
	$_SESSION["own_pins"][] = $newPinID;

	respondOK([
		"new_pin_id" => intval($newPinID),
	]);
}

function editPin()
{
	global $pdo;

	$data = json_decode(file_get_contents("php://input"), true);
	checkPinType($type = getRequiredArg("type", $data));
	checkPinOwnership($id = getRequiredInt("id", $data));

	$stmt = $pdo->prepare(<<<SQL
UPDATE Pin
SET type = :type,
    title = :title,
    body = :body
WHERE id = :id
SQL
	);

	$stmt->bindValue(":type", $type);
	$stmt->bindValue(":id", getRequiredInt("id", $data), PDO::PARAM_INT);
	$stmt->bindValue(":title", getRequiredArg("title", $data));
	$stmt->bindValue(":body", getRequiredArg("body", $data));

	if (!$stmt->execute()) {
		error_log("Error updating pin: " . $stmt->errorCode());
		respondError([
			"error" => "failed to update pin",
		]);
	}

	respondOK();
}

function deletePin() {
	global $pdo;

	checkPinOwnership($id = getRequiredInt("id"));

	$stmt = $pdo->prepare("DELETE FROM Pin WHERE id = :id");
	$stmt->bindValue(":id", $id, PDO::PARAM_INT);

	if (!$stmt->execute()) {
		error_log("Error deleting pin: " . $stmt->errorCode());
		respondError([
			"error" => "failed to delete pin",
		]);
	}

	respondOK();
}
