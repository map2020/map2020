<?php

$ALLOWED_PIN_TYPES = [
	"generic",
	"protest",
	"violence",
	"covid-19",
];

/**
 * Get a value from an associative array or respond with an error.
 * @param string $key : The name of the variable to get
 * @param array|null $haystack : Where to search (defaults to $_GET)
 * @return string: The value
 */
function getRequiredArg(string $key, array $haystack = null) : string
{
	$value = null;

	if (is_null($haystack)) {
		$haystack = $_GET;
	}

	if (!isset($haystack[$key]) || !strlen($value = $haystack[$key])) {
		respondError([
			"error" => "missing required parameter",
			"parameter" => $key,
		], 400);
	}

	return $value;
}

/**
 * Get a float from an associative array or respond with an error.
 * @param string $key : The name of the variable to get
 * @param array|null $haystack : Where to search (defaults to $_GET)
 * @return float: The value
 */
function getRequiredFloat(string $key, array $haystack = null) : float
{
	$value = getRequiredArg($key, $haystack);

	if (!is_numeric($value) || !is_float($value = floatval($value))) {
		respondError([
			"error" => "parameter must be a double",
			"parameter" => $key,
		], 400);
	}

	return $value;
}

/**
 * Get an int from an associative array or respond with an error.
 * @param string $key : The name of the variable to get
 * @param array|null $haystack : Where to search (defaults to $_GET)
 * @return int: The value
 */
function getRequiredInt(string $key, array $haystack = null) : int
{
	$value = getRequiredArg($key, $haystack);

	if (!is_numeric($value) || !is_int($value = intval($value))) {
		respondError([
			"error" => "parameter must be an int",
			"parameter" => $key,
		], 400);
	}

	return $value;
}

/**
 * Respond with an error if the pin type is not known.
 * @param string $type: the pin type that was submitted
 */
function checkPinType(string $type)
{
	global $ALLOWED_PIN_TYPES;

	if (!in_array($type, $ALLOWED_PIN_TYPES)) {
		respondError([
			"error" => "that pin type is not allowed",
		], 400);
	}
}

/**
 * Respond with an error if the current user cannot edit this pin.
 * @param int $id: the ID of the pin to check
 */
function checkPinOwnership(int $id) {
	if (!in_array($id, $_SESSION["own_pins"])) {
		respondError([
			"error" => "you do not have permission to edit that pin",
		], 403);
	}
}

/**
 * Respond with a successful status code and terminate execution.
 * @param array $data : Data to include with the response
 * @param int $status : The status code (defaults to 200)
 */
function respondOK(array $data = [], int $status = 200) : void
{
	header("Content-Type: application/json");
	http_response_code($status);
	die(json_encode($data));
}

/**
 * Respond with an error status code and terminate execution.
 * @param array|string[] $data : Data to include with the response
 * @param int $status : The status code (defaults to 500)
 */
function respondError(array $data = ["error" => "Unspecified error"], int $status = 500) : void
{
	header("Content-Type: application/json");
	http_response_code($status);
	die(json_encode($data));
}

/**
 * Respond with an error because of an invalid HTTP method.
 * @param array $allowedMethods : The list of methods that could be used instead
 */
function respondInvalidMethod(array $allowedMethods) : void
{
	respondError([
		"error" => "invalid HTTP method",
		"method" => $_SERVER["REQUEST_METHOD"],
		"accepted_methods" => $allowedMethods,
	], 405);
}
