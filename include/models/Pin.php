<?php

class Pin implements JsonSerializable
{
	public int $id;
	public string $dt;
	public string $type;
	public string $title;
	public string $body;
	public float $lat;
	public float $lng;
	public string $own;

	public function jsonSerialize()
	{
		try {
			$date = new DateTime($this->dt);
		} catch (Exception $e) {
			$date = new DateTime();
		}

		return [
			"id" => $this->id,
			"dt" => $date->getTimestamp(),
			"type" => $this->type,
			"title" => $this->title,
			"body" => $this->body,
			"lat" => floatval($this->lat),
			"lng" => floatval($this->lng),
			"own" => boolval($this->own),
		];
	}
}
