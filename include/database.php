<?php

try {
	require_once "{$_SERVER['DOCUMENT_ROOT']}/../db.php";
	$pdo = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
} catch (PDOException $exception) {
	error_log("Could not connect to database: " . trim($exception->getMessage()));
	respondError([
		"error" => "Could not connect to database",
	]);
}
